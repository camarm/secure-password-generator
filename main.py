import tkinter
import string
from random import randint, choice
from tkinter import *
import webbrowser

def generate():
    password_min = 10
    password_max = 18
    all_chars = string.ascii_letters + string.punctuation + string.digits
    password = "".join(choice(all_chars) for x in range(randint(password_min, password_max)))
    input.delete(0, END)
    input.insert(0, password)

def open_page():
    webbrowser.open_new("https://gitlab.com/-/ide/project/CAMARM_Flipz/secure-password-generator/tree/master/-/main.py/")

window =Tk()
window.title("Générateur de mot de passe sécurisés")
window.geometry("720x480")
window.iconbitmap("password.ico")
window.config(background='#1c8fbb')

frame = Frame(window, bg='#1c8fbb')

width = 312
height = 312
image = PhotoImage(file="password.png").zoom(19).subsample(32)
canvas = Canvas(frame, width=width, height=height, bg='#1c8fbb', bd=0, highlightthickness=0)
canvas.create_image(width/2, height/2, image=image)
canvas.grid(row=0, column=0, sticky=W)

right_frame = Frame(frame, bg='#1c8fbb')

title = Label(right_frame, text="Mot de passe", font=("Courrier", 25), bg='#1c8fbb', fg='white')
title.pack()

input = Entry(right_frame, font=("Courrier", 25), bg='#1c8fbb', fg='white')
input.pack()

button = Button(right_frame, text="Générer un mot de passe", font=("Courrier", 22), bg='#1c8fbb', fg='white', command=generate)
button.pack(fill=X)

right_frame.grid(row=0, column=1, sticky=W)

frame.pack(expand=YES)

menu = Menu(window)
file_menu = Menu(menu, tearoff=0)
file_menu.add_command(label="Nouveau", command=generate)
menu.add_cascade(label="File", menu=file_menu)
quit_menu = Menu(menu, tearoff=0)
quit_menu.add_command(label="Quitter", command=window.quit)
menu.add_cascade(label="Quit", menu=quit_menu)
other_menu = Menu(menu, tearoff=0)
other_menu.add_command(label="Télécharger le script", command=open_page)
menu.add_cascade(label="Other", menu=other_menu)

window.config(menu=menu)

window.mainloop()
